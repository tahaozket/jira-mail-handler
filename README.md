# Custom Message Mail Handler for Jira

This project is a tutorial for developers learning to write a message handler for JIRA.
You can find the entire tutorial at: [Custom Message Mail Handler for Jira][1].

## Running locally

To run this app locally, make sure that you have the Atlassian Plugin SDK installed, and then run:

    atlas-mvn jira:run

 [1]: https://developer.atlassian.com/display/JIRADEV/Custom+Message+%28Mail%29+Handler+for+JIRA