package com.example.plugins.tutorial.jira.mailhandlerdemo;

import javax.mail.Message;
import javax.mail.MessagingException;

import com.atlassian.jira.service.util.handler.MessageHandlerContext;

public class DemoHandler extends CreateOrCommentHandler {
	
	@Override
	public boolean handleMessage(Message message, MessageHandlerContext context) throws MessagingException {
		boolean value = super.handleMessage(message, context);
		
		issueType = "Task";
		projectKey = "TEST";
		createUsers = false;
		defaultReporterUserKey="admin";
		defaultReporterUserName="admin";

	
		return value;
	}

}