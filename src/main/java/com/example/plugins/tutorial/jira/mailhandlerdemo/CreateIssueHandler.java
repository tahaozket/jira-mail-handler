package com.example.plugins.tutorial.jira.mailhandlerdemo;

import com.atlassian.jira.JiraApplicationContext;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.event.mau.MauEventService;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFactory;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.ModifiedValue;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.SummaryField;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.priority.Priority;
import com.atlassian.jira.issue.security.IssueSecurityLevelManager;
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder;
import com.atlassian.jira.issue.util.IssueChangeHolder;
import com.atlassian.jira.issue.util.IssueUpdater;
import com.atlassian.jira.issue.watchers.WatcherManager;
import com.atlassian.jira.mail.MailLoggingManager;
import com.atlassian.jira.mail.MailThreadManager;
import com.atlassian.jira.mail.MailThreadManager.MailAction;
import com.atlassian.jira.plugins.mail.handlers.AbstractMessageHandler;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.service.util.handler.MessageHandlerContext;
import com.atlassian.jira.service.util.handler.MessageHandlerErrorCollector;
import com.atlassian.jira.service.util.handler.MessageHandlerExecutionMonitor;
import com.atlassian.jira.service.util.handler.MessageUserProcessor;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.UserUtils;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.web.FieldVisibilityManager;
import com.atlassian.jira.web.action.issue.IssueCreationHelperBean;
import com.atlassian.jira.web.bean.I18nBean;
import com.atlassian.mail.MailUtils;
import com.google.common.annotations.VisibleForTesting;
import com.opensymphony.util.TextUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Part;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.ofbiz.core.entity.GenericValue;


public class CreateIssueHandler extends AbstractMessageHandler {
	public static final String KEY_PROJECT = "project";
	public static final String KEY_ISSUETYPE = "issuetype";
	public static final String CC_ASSIGNEE = "ccassignee";
	public static final String CC_WATCHER = "ccwatcher";
	public String projectKey;
	public String issueType;
	public static final boolean DEFAULT_CC_ASSIGNEE = true;
	public boolean ccAssignee = true;
	public boolean ccWatcher = false;

	public CreateIssueHandler() {
	}

	@VisibleForTesting
	CreateIssueHandler(PermissionManager permissionManager, IssueUpdater issueUpdater, UserManager userManager,
			ApplicationProperties applicationProperties, JiraApplicationContext jiraApplicationContext,
			MailLoggingManager mailLoggingManager, MessageUserProcessor messageUserProcessor) {
		super(userManager, applicationProperties, jiraApplicationContext, mailLoggingManager, messageUserProcessor,
				permissionManager);
	}

	public void init(Map<String, String> params, MessageHandlerErrorCollector errorCollector) {
		this.log.debug("CreateIssueHandler.init(params: " + params + ")");

		super.init(params, errorCollector);
		if (params.containsKey("project")) {
			this.projectKey = ((String) params.get("project"));
		}
		if (params.containsKey("issuetype")) {
			this.issueType = ((String) params.get("issuetype"));
		}
		if (params.containsKey("ccassignee")) {
			this.ccAssignee = Boolean.valueOf((String) params.get("ccassignee")).booleanValue();
		}
		if (params.containsKey("ccwatcher")) {
			this.ccWatcher = Boolean.valueOf((String) params.get("ccwatcher")).booleanValue();
		}
	}

	public boolean handleMessage(Message message, MessageHandlerContext context) throws MessagingException {
		this.log.debug("CreateIssueHandler.handleMessage");
		if (!canHandleMessage(message, context.getMonitor())) {
			return this.deleteEmail;
		}
		try {
			ApplicationUser reporter = getReporter(message, context);
			if (reporter == null) {
				String error = getI18nBean().getText("admin.mail.no.default.reporter");
				context.getMonitor().warning(error);
				context.getMonitor().messageRejected(message, error);
				return false;
			}
			Project project = getProject(message);

			this.log.debug("Project = " + project);
			if (project == null) {
				String text = getI18nBean().getText("admin.mail.no.project.configured");
				context.getMonitor().warning(text);
				context.getMonitor().messageRejected(message, text);
				return false;
			}
			ErrorCollection errorCollection = new SimpleErrorCollection();

			I18nHelper i18nHelper = new I18nBean(Locale.ENGLISH);

			getIssueCreationHelperBean().validateLicense(reporter, errorCollection, i18nHelper);
			if (errorCollection.hasAnyErrors()) {
				context.getMonitor().warning(
						getI18nBean().getText("admin.mail.bad.license", errorCollection.getErrorMessages().toString()));
				return false;
			}
			if ((!getPermissionManager().hasPermission(11, project, reporter, true))
					&& (reporter.getDirectoryId() != -1L)) {
				String error = getI18nBean().getText("admin.mail.no.create.permission", reporter.getName());
				context.getMonitor().warning(error);
				context.getMonitor().messageRejected(message, error);
				return false;
			}
			this.log.debug("Issue Type Key = " + this.issueType);
			if (!hasValidIssueType()) {
				context.getMonitor().warning(getI18nBean().getText("admin.mail.invalid.issue.type"));
				return false;
			}
			String summary = message.getSubject();
			if (!TextUtils.stringSet(summary)) {
				context.getMonitor().markMessageForDeletion(getI18nBean().getText("admin.mail.no.subject"));
				return false;
			}
			if (summary.length() > SummaryField.MAX_LEN.intValue()) {
				context.getMonitor().info("Truncating summary field because it is too long: " + summary);
				summary = summary.substring(0, SummaryField.MAX_LEN.intValue() - 3) + "...";
			}
			String priority = null;
			String description = null;
			if (!getFieldVisibilityManager().isFieldHiddenInAllSchemes(project.getId(), "priority",
					Collections.singletonList(this.issueType))) {
				priority = getPriority(message);
			}
			if (!getFieldVisibilityManager().isFieldHiddenInAllSchemes(project.getId(), "description",
					Collections.singletonList(this.issueType))) {
				description = getDescription(reporter, message);
			}
			ApplicationUser assignee = null;
			if (this.ccAssignee) {
				assignee = getFirstValidAssignee(message.getAllRecipients(), project);
			}
			if (assignee == null) {
				assignee = getProjectManager().getDefaultAssignee(project, Collections.EMPTY_SET);
			}
			MutableIssue issueObject = getIssueFactory().getIssue();
			issueObject.setProjectObject(project);
			issueObject.setSummary(summary);
			issueObject.setDescription(description);
			issueObject.setIssueTypeId(this.issueType);
			issueObject.setReporter(reporter);
			if (assignee != null) {
				issueObject.setAssignee(assignee);
			}
			issueObject.setPriorityId(priority);

			setDefaultSecurityLevel(issueObject);

			Map<String, Object> fields = new HashMap();
			fields.put("issue", issueObject);

			MutableIssue originalIssue = getIssueManager().getIssueObject(issueObject.getId());

			List<CustomField> customFieldObjects = ComponentAccessor.getCustomFieldManager()
					.getCustomFieldObjects(issueObject);
			for (CustomField customField : customFieldObjects) {
				issueObject.setCustomFieldValue(customField, customField.getDefaultValue(issueObject));
			}

			/*
			 *  Setting sender email as a custom field			
			 */
			
			CustomFieldManager manager = ComponentAccessor.getCustomFieldManager();
			CustomField mailCustomField = manager.getCustomFieldObject(14823L);
			Address[] froms = message.getFrom();
			String email = froms == null ? null : ((InternetAddress) froms[0]).getAddress();
			issueObject.setCustomFieldValue(mailCustomField, email);

			/* 
			 * End
			 */
			
			fields.put("originalissueobject", originalIssue);
			Issue issue = context.createIssue(reporter, issueObject);

			this.mauEventService.setApplicationForThreadBasedOnProject(project);
			
			addCcWatchersAndAttachments(message, context, issue, reporter);

		} catch (Exception e) {
			context.getMonitor().warning(getI18nBean().getText("admin.mail.unable.to.create.issue"), e);
			return false;
		}

		return true;
	}

	@VisibleForTesting
	void addCcWatchersAndAttachments(Message message, MessageHandlerContext context, Issue issue,
			ApplicationUser reporter) {
		try {
			if (issue != null) {
				if (this.ccWatcher) {
					addCcWatchersToIssue(message, issue, reporter, context, context.getMonitor());
				}
				recordIncomingMessageId(MailThreadManager.MailAction.ISSUE_CREATED_FROM_EMAIL, message, issue, context);

				createAttachmentsForMessage(message, issue, context);
			}
		} catch (AddressException ae) {
			addCommentIndicatingWatcherFailureAndMarkForDeletion(issue, context);
		} catch (MessagingException me) {
			addCommentIndicatingAttachmentFailureAndMarkForDeletion(issue, context, me);
		} catch (IOException ie) {
			addCommentIndicatingAttachmentFailureAndMarkForDeletion(issue, context, ie);
		}
	}

	@VisibleForTesting
	void addCommentIndicatingWatcherFailureAndMarkForDeletion(Issue issue, MessageHandlerContext context) {
		context.createComment(issue, null, getI18nBean().getText("jmp.handler.watchers.failed"), true);
		context.getMonitor().markMessageForDeletion(getI18nBean().getText("jmp.handler.watchers.failed"));
	}

	private IssueCreationHelperBean getIssueCreationHelperBean() {
		return (IssueCreationHelperBean) ComponentAccessor.getComponent(IssueCreationHelperBean.class);
	}

	PermissionManager getPermissionManager() {
		return ComponentAccessor.getPermissionManager();
	}

	FieldVisibilityManager getFieldVisibilityManager() {
		return (FieldVisibilityManager) ComponentAccessor.getComponent(FieldVisibilityManager.class);
	}


	IssueManager getIssueManager() {
		return ComponentAccessor.getIssueManager();
	}

	IssueFactory getIssueFactory() {
		return ComponentAccessor.getIssueFactory();
	}

	public void addCcWatchersToIssue(Message message, Issue issue, ApplicationUser reporter,
			MessageHandlerContext context, MessageHandlerExecutionMonitor messageHandlerExecutionMonitor)
					throws MessagingException {
		Collection<ApplicationUser> users = getAllUsersFromEmails(message.getAllRecipients());
		users.remove(reporter);
		if (!users.isEmpty()) {
			Issue updatedIssue;
			if (context.isRealRun()) {
				updatedIssue = issue;
				for (ApplicationUser user : users) {
					updatedIssue = getWatcherManager().startWatching(user, updatedIssue);
				}
			} else {
				String watchers = StringUtils.join(users, ",");
				messageHandlerExecutionMonitor.info("Adding watchers " + watchers);
				this.log.debug("Watchers [" + watchers + "] not added due to dry-run mode.");
			}
		}
	}

	WatcherManager getWatcherManager() {
		return ComponentAccessor.getWatcherManager();
	}

	public Collection<ApplicationUser> getAllUsersFromEmails(Address[] addresses) {
		if ((addresses == null) || (addresses.length == 0)) {
			return Collections.emptyList();
		}
		List<ApplicationUser> users = new ArrayList();
		for (Address address : addresses) {
			String emailAddress = getEmailAddress(address);
			if (emailAddress != null) {
				ApplicationUser user = UserUtils.getUserByEmail(emailAddress);
				if (user != null) {
					users.add(user);
				}
			}
		}
		return users;
	}

	private String getEmailAddress(Address address) {
		if ((address instanceof InternetAddress)) {
			InternetAddress internetAddress = (InternetAddress) address;
			return internetAddress.getAddress();
		}
		return null;
	}

	protected Project getProject(Message message) {
		if (this.projectKey == null) {
			this.log.debug("Project key NOT set. Cannot find project.");
			return null;
		}
		this.log.debug("Project key = " + this.projectKey);

		return getProjectManager().getProjectObjByKey(this.projectKey.toUpperCase(Locale.getDefault()));
	}

	protected boolean hasValidIssueType() {
		if (this.issueType == null) {
			this.log.debug("Issue Type NOT set. Cannot find Issue type.");
			return false;
		}
		IssueType issueTypeObject = getConstantsManager().getIssueTypeObject(this.issueType);
		if (issueTypeObject == null) {
			this.log.debug("Issue Type with does not exist with id of " + this.issueType);
			return false;
		}
		this.log.debug("Issue Type Object = " + issueTypeObject.getName());
		return true;
	}

	protected ProjectManager getProjectManager() {
		return ComponentAccessor.getProjectManager();
	}

	private String getDescription(ApplicationUser reporter, Message message) throws MessagingException {
		return recordFromAddressForAnon(reporter, message, MailUtils.getBody(message));
	}

	@VisibleForTesting
	String recordFromAddressForAnon(ApplicationUser reporter, Message message, String description)
			throws MessagingException {
		if ((this.defaultReporterUserName != null) && (this.defaultReporterUserName.equalsIgnoreCase(reporter.getName()))) {
			description = description + "\n[Created via e-mail ";
			if ((message.getFrom() != null) && (message.getFrom().length > 0)) {
				description = description + "received from: " + message.getFrom()[0] + "]";
			} else {
				description = description + "but could not establish sender's address.]";
			}
		}
		return description;
	}

	private String getPriority(Message message) throws MessagingException {
		String[] xPrioHeaders = message.getHeader("X-Priority");
		if ((xPrioHeaders != null) && (xPrioHeaders.length > 0)) {
			String xPrioHeader = xPrioHeaders[0];

			int priorityValue = Integer.parseInt(TextUtils.extractNumber(xPrioHeader));
			if (priorityValue == 0) {
				return getDefaultSystemPriority();
			}
			Collection<Priority> priorities = getConstantsManager().getPriorityObjects();

			Iterator<Priority> priorityIt = priorities.iterator();

			int priorityNumber = (int) Math.ceil(priorityValue / 5.0D * priorities.size());
			if (priorityNumber > priorities.size()) {
				priorityNumber = priorities.size();
			}
			String priority = null;
			for (int i = 0; i < priorityNumber; i++) {
				priority = ((Priority) priorityIt.next()).getId();
			}
			return priority;
		}
		return getDefaultSystemPriority();
	}

	ConstantsManager getConstantsManager() {
		return (ConstantsManager) ComponentAccessor.getConstantsManager();
	}

	private String getDefaultSystemPriority() {
		Priority defaultPriority = getConstantsManager().getDefaultPriorityObject();
		if (defaultPriority == null) {
			this.log.warn("Default priority was null. Using the 'middle' priority.");
			Collection<Priority> priorities = getConstantsManager().getPriorityObjects();
			int times = (int) Math.ceil(priorities.size() / 2.0D);
			Iterator<Priority> priorityIt = priorities.iterator();
			for (int i = 0; i < times; i++) {
				defaultPriority = (Priority) priorityIt.next();
			}
		}
		if (defaultPriority == null) {
			throw new RuntimeException("Default priority not found");
		}
		return defaultPriority.getId();
	}

	public static ApplicationUser getFirstValidAssignee(Address[] addresses, Project project) {
		if ((addresses == null) || (addresses.length == 0)) {
			return null;
		}
		for (Address address : addresses) {
			if ((address instanceof InternetAddress)) {
				InternetAddress email = (InternetAddress) address;

				ApplicationUser validUser = UserUtils.getUserByEmail(email.getAddress());
				if ((validUser != null)
						&& (((PermissionManager) ComponentAccessor.getPermissionManager())
								.hasPermission(17, project, validUser))) {
					return validUser;
				}
			}
		}
		return null;
	}

	private void setDefaultSecurityLevel(MutableIssue issue) throws Exception {
		GenericValue project = issue.getProject();
		if (project != null) {
			Long levelId = getIssueSecurityLevelManager().getSchemeDefaultSecurityLevel(project);
			if (levelId != null) {
				issue.setSecurityLevel(getIssueSecurityLevelManager().getIssueSecurity(levelId));
			}
		}
	}

	IssueSecurityLevelManager getIssueSecurityLevelManager() {
		return (IssueSecurityLevelManager) ComponentAccessor.getIssueSecurityLevelManager();
	}

	protected boolean attachPlainTextParts(Part part) throws MessagingException, IOException {
		return (!MailUtils.isContentEmpty(part)) && (MailUtils.isPartAttachment(part));
	}

	protected boolean attachHtmlParts(Part part) throws MessagingException, IOException {
		return (!MailUtils.isContentEmpty(part)) && (MailUtils.isPartAttachment(part));
	}
}