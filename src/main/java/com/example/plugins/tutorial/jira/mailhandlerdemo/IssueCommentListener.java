package com.example.plugins.tutorial.jira.mailhandlerdemo;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.annotation.Nonnull;
import javax.inject.Named;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;

import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.event.issue.AbstractIssueEventListener;
import com.atlassian.jira.event.issue.DelegatingJiraIssueEvent;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.event.issue.IssueEventBundle;
import com.atlassian.jira.event.issue.JiraIssueEvent;
import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.issue.AttachmentManager;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.mail.IssueMailQueueItemFactory;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.scheme.SchemeEntity;
import com.atlassian.jira.template.TemplateManager;
import com.atlassian.jira.util.AttachmentUtils;
import com.atlassian.mail.Email;
import com.atlassian.mail.queue.MailQueue;
import com.atlassian.mail.queue.SingleMailQueueItem;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

@Named
public class IssueCommentListener extends AbstractIssueEventListener implements InitializingBean, DisposableBean {
	
    private static final Logger log = LoggerFactory.getLogger(IssueCommentListener.class);

    @ComponentImport
    @Autowired
	private TemplateManager templateManager;

    @ComponentImport
    @Autowired
	private MailQueue mailQueue;

    @ComponentImport
    @Autowired
	private IssueMailQueueItemFactory issueMailQueueItemFactory;
	
    @ComponentImport
    @Autowired
	private final EventPublisher eventPublisher;

    @Autowired
	public IssueCommentListener(EventPublisher eventPublisher, TemplateManager templateManager,
			IssueMailQueueItemFactory issueMailQueueItemFactory, MailQueue mailQueue) {
		this.issueMailQueueItemFactory = issueMailQueueItemFactory;
		this.mailQueue = mailQueue;
		this.templateManager = templateManager;
		this.eventPublisher = eventPublisher;

	}

	@Override
	protected void handleDefaultIssueEvent(IssueEvent event) {
		if (event.isRedundant()) {
			return;
		}
		super.handleDefaultIssueEvent(event);
	}

	@EventListener
	public void handleIssueEventBundle(IssueEventBundle issueEventBundle) {

		if (!issueEventBundle.doesSendEmailNotification()) {
			return;
		}

		IssueEvent selectedEvent = null;
		for (JiraIssueEvent event : issueEventBundle.getEvents()) {
			if ((event instanceof DelegatingJiraIssueEvent)) {
				IssueEvent issueEvent = ((DelegatingJiraIssueEvent) event).asIssueEvent();
				if (issueEvent.isSendMail()) {
					

					if (issueEvent.getEventTypeId() == EventType.ISSUE_COMMENTED_ID || 
							issueEvent.getEventTypeId() == EventType.ISSUE_COMMENT_EDITED_ID ||
							issueEvent.getEventTypeId() == EventType.ISSUE_RESOLVED_ID ||
							issueEvent.getEventTypeId() == 2L) {
						selectedEvent = issueEvent;
					}
				}
			}
		}
		
		try {
			
			if (selectedEvent != null){
				CustomFieldManager manager = ComponentAccessor.getCustomFieldManager();
				AttachmentManager attachmentManager = ComponentAccessor.getAttachmentManager();
				
				CustomField mailCustomField = manager.getCustomFieldObject(14823L);
//				CustomField mailCustomField = manager.getCustomFieldObject(10007L);
				String email = (String) selectedEvent.getIssue().getCustomFieldValue(mailCustomField);

				Email em = new Email(email);

				List<String> attachmentIds = new ArrayList<>();
				if (selectedEvent.getChangeLog() != null) {
					List<GenericValue> changeItems = selectedEvent.getChangeLog().getRelated("ChildChangeItem");
					for (GenericValue genericValue : changeItems) {
						if ("Attachment".equals(genericValue.get("field")) && genericValue.get("newvalue")!=null) {
							attachmentIds.add(genericValue.getString("newvalue"));
						}
					}					
				}

				if (!"".equals(selectedEvent.getIssue().getProjectObject().getEmail())) {
					em.setFrom(selectedEvent.getIssue().getProjectObject().getEmail());
				}

				if (!attachmentIds.isEmpty()) {
					
	                MimeMultipart mp = new MimeMultipart("mixed");
	   
	    			for (String attachmentId:attachmentIds){
	    				Attachment emailAttachment = attachmentManager.getAttachment(Long.valueOf(attachmentId));
		                File attFil = AttachmentUtils.getAttachmentFile(emailAttachment);
		    			MimeBodyPart bodyPart = new MimeBodyPart();
		    			bodyPart.setFileName(MimeUtility.encodeText(emailAttachment.getFilename()));
		    			FileDataSource attFds = new FileDataSource(attFil);
		    			bodyPart.setDataHandler(new DataHandler(attFds));
		    			mp.addBodyPart(bodyPart);
	    			}
	    			
					em.setSubject(selectedEvent.getIssue().getKey() + " File Attached");
	    			em.setMultipart(mp);
				} else {
					em.setSubject(selectedEvent.getIssue().getKey() + " Commented");
					em.setBody(selectedEvent.getComment().getBody());
				}
				
				SingleMailQueueItem smqi = new SingleMailQueueItem(em);
				this.mailQueue.addItem(smqi);

			}
			
		} catch (Exception e) {
			log.error("IssueListener",e);
		}
		


	}

	@Nonnull
	private List<SchemeEntity> getSchemeEntities(@Nonnull Project project, @Nonnull IssueEvent event) {
		try {
			return ComponentAccessor.getNotificationSchemeManager().getNotificationSchemeEntities(project,
					event.getEventTypeId().longValue());
		} catch (GenericEntityException e) {

		}
		return Collections.emptyList();
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		// register ourselves with the EventPublisher
		eventPublisher.register(this);
	}

	/**
	 * Called when the plugin is being disabled or removed.
	 *
	 * @throws Exception
	 */
	@Override
	public void destroy() throws Exception {
		// unregister ourselves with the EventPublisher
		eventPublisher.unregister(this);
	}

}
